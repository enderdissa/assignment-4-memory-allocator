#include <assert.h>
#include <fcntl.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/mman.h>

#include "mem.h"
#include "mem_internals.h"

#define MIN_BLOCK_SIZE 24
#define TEST_HEAP_SIZE 4096


static struct block_header* header_from_ptr(void* ptr) {
    return (struct block_header*)((uint8_t*)ptr - offsetof(struct block_header, contents));
}


static void allocation() {
	printf("memory allocation test:\n");
    void* heap_start = heap_init(TEST_HEAP_SIZE);
    assert(heap_start);

    void* allocated_block = _malloc(100);
    assert(allocated_block != NULL);
    heap_term();

    printf("SUCCES\n\n");
}

static void free_single_block() {
	printf("free one block test:\n");
    void* test_heap = heap_init(TEST_HEAP_SIZE);
    assert(test_heap);

    void* test_block1 = _malloc(200);
    assert(test_block1 != NULL);
    void* test_block2 = _malloc(100);
    assert(test_block2 != NULL);

    _free(test_block2);
    struct block_header* block_header1 = block_get_header(test_block1);
    struct block_header* block_header2 = block_get_header(test_block2);

    assert(block_header1->next == block_header2);
    assert(block_header2->is_free);

    _free(test_block1);
    if (test_block1) {
      fprintf(stderr, "FAIL! blocks were not free\n");
    }
    heap_term();

    printf("SUCCES\n\n");
}

static void free_two_blocks() {
	printf("free two blocks test:\n");

    void* test_heap = heap_init(TEST_HEAP_SIZE);
    assert(test_heap != NULL);

    void* test_block1 = _malloc(150);
    assert(test_block1 != NULL);
    void* test_block2 = _malloc(300);
    assert(test_block2 != NULL);
    void* test_block3 = _malloc(600);
    assert(test_block3 != NULL);

    _free(test_block1);
    if (test_block1) {
        fprintf(stderr, "FAIL! blocks were not free\n");
    }
    _free(test_block3);
        if (test_block3) {
      fprintf(stderr, "FAIL! blocks were not free\n");
    }

    struct block_header* block_header1 = block_get_header(test_block1);
    struct block_header* block_header2 = block_get_header(test_block2);
    struct block_header* block_header3 = block_get_header(test_block3);

    assert(block_header1->is_free);
    assert(!(block_header2->is_free));
	assert(block_header3->is_free);

    _free(test_block2);
    if (test_block2) {
      fprintf(stderr, "FAIL! blocks were not free\n");
    }
    heap_term();

    printf("SUCCES\n\n");
}


static void expand_region() {
	printf("expanding region test:\n");

    void* test_heap = heap_init(TEST_HEAP_SIZE);
    assert(test_heap);

    void* test_block1 = _malloc(TEST_HEAP_SIZE);
    assert(test_block1 != NULL);

    void* test_block2 = _malloc(TEST_HEAP_SIZE*5);
    assert(test_block2 != NULL);

    _free(test_block2);
    _free(test_block1);
    heap_term();

    printf("SUCCES\n\n");
}


static void allocate_new_region() {
    printf("allocate another region test:\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);

    void* new_block = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header* new_block_header = header_from_ptr(new_block);

    assert(new_block);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= (REGION_MIN_SIZE - offsetof(struct block_header, contents)));

    munmap(hole, 100);
    _free(new_block);
    heap_term();

    printf("SUCCES\n\n");
}

struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

int main(){
	allocation();
	free_single_block();
	free_two_blocks();
	expand_region();
	allocate_new_region();

	return 0;
}
